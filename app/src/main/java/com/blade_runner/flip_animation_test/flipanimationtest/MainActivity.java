package com.blade_runner.flip_animation_test.flipanimationtest;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private AppCompatImageView imageToAnimate;
    private AppCompatButton btnStartFlipAnimation, btnStopFlipAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.initializeUI();
        this.initializeAnimator();
    }

    private void initializeUI() {
        setContentView(R.layout.activity_main);
        this.imageToAnimate = findViewById(R.id.imageToAnimate);
        this.btnStartFlipAnimation = findViewById(R.id.btnStartFlipAnimation);
        this.btnStopFlipAnimation = findViewById(R.id.btnStopFlipAnimation);
        this.btnStartFlipAnimation.setOnClickListener(this);
        this.btnStopFlipAnimation.setOnClickListener(this);
    }

    private void initializeAnimator() {
        this.flipAnimation = ObjectAnimator.ofFloat(this.imageToAnimate, "rotationY", 0.0f, 1440f);
//        enable lines of code given below to introudce a skew/scale animation
//        Animation logoMoveAnimation = AnimationUtils.loadAnimation(this, R.anim.scale_animation);
//        this.imageToAnimate.startAnimation(logoMoveAnimation);
    }

    private ObjectAnimator flipAnimation;
    private void startFlipAnimation() {
        this.flipAnimation.setDuration(4000);
        this.flipAnimation.setRepeatCount(ValueAnimator.INFINITE);
        this.imageToAnimate.setCameraDistance(10 * this.imageToAnimate.getWidth());
        this.flipAnimation.start();
    }

    private void stopFlipAnimation() {
        if(this.flipAnimation.isStarted()) {
            if(this.flipAnimation.isRunning()) {
                this.flipAnimation.cancel();
                this.flipAnimation.setDuration(0);
                this.flipAnimation.setRepeatCount(0);
                this.flipAnimation.reverse();
            }
        } else {
            Toast.makeText(this, "Start animation first!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnStartFlipAnimation:
                this.startFlipAnimation();
                break;
            case R.id.btnStopFlipAnimation:
                this.stopFlipAnimation();
                break;
        }
    }
}
